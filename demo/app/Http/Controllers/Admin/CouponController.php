<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\CreateCouponRequest;
use App\Http\Requests\Coupons\UpdateCouponRequest;
use Illuminate\Http\Request;
use App\Models\Coupon;
use Carbon\Carbon;

class CouponController extends Controller
{
    protected $coupon;

    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    public function index()
    {
        $coupons =  $this->coupon->latest('id')->paginate(5);
        return view('admin.coupons.index', compact('coupons'));

    }

    public function create()
    {
        return view('admin.coupons.create');
    }

    public function store(CreateCouponRequest $request)
    {
        $dataCreate =  $request->all();
        $this->coupon->create($dataCreate);
        return redirect()->route('coupons.index')->with(['message' => 'Create  success']);
    }

    public function edit($id)
    {
        $coupon = $this->coupon->findOrFail($id);
        return view('admin.coupons.edit', compact('coupon'));
    }

    public function update(UpdateCouponRequest $request, $id)
    {
        $coupon = $this->coupon->findOrFail($id);
        $dataUpdate = $request->all();
        $coupon->update($dataUpdate);
        return redirect()->route('coupons.index')->with(['message' => 'Update  success']);
    }

    public function destroy($id)
    {
        $coupon = $this->coupon->findOrFail($id);
        $coupon->delete();
        return redirect()->route('coupons.index')->with(['message' => 'Delete success']);
    }
}
