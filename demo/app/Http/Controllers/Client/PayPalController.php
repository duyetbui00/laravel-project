<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\CreateOrderRequest;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Symfony\Component\Console\Input\Input;

class PayPalController extends Controller
{

    protected $cart;
    protected $product;
    protected $cartProduct;
    protected $coupon;
    protected $order;

    public function __construct(Product $product, Cart $cart, CartProduct $cartProduct, Coupon $coupon, Order $order)
    {
        $this->product = $product;
        $this->cart = $cart;
        $this->cartProduct = $cartProduct;
        $this->coupon = $coupon;
        $this->order = $order;
    }

    public function createTransaction()
    {
        return view('client.carts.testpaypal');
    }

    public function processTransaction(CreateOrderRequest $request)
    {

        if($request->input('payment')=='money'){
            $dataCreate = $request->all();
            $dataCreate['user_id'] = auth()->user()->id;
            $dataCreate['status'] = 'Pending';
            $this->order->create($dataCreate);
            $couponID = Session::get('coupon_id');
            if($couponID)
            {
                $coupon =  $this->coupon->find(Session::get('coupon_id'));
                if($coupon)
                {
                    $coupon->users()->attach(auth()->user()->id, ['value' => $coupon->value]);
                }
            }
            $cart = $this->cart->firtOrCreateBy(auth()->user()->id);
            $cart->products()->delete();
            Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);
            return redirect('list-orders')->with('success', 'Order success');;
        }else if($request->input('payment')=='paypal'){
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $paypalToken = $provider->getAccessToken();
        $response = $provider->createOrder([
            "intent" => "CAPTURE",
            "application_context" => [
                "return_url" => route('successTransaction',$request),
                "cancel_url" => route('cancelTransaction'),
            ],
            "purchase_units" => [
                0 => [
                    "amount" => [
                        "currency_code" => "USD",
                        "value" => $request->input('total'),
                    ]
                ]
            ]
        ]);

        if (isset($response['id']) && $response['id'] != null) {

            // redirect to approve href
            foreach ($response['links'] as $links) {
                if ($links['rel'] == 'approve') {
                    return redirect()->away($links['href']);
                }
            }

            return redirect()
                ->route('client.orders.index')
                ->with('error', 'Something went wrong.');

        } else {
            return redirect()
                ->route('client.orders.index')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }else{
            return back();
        }
    }

    public function successTransaction(Request $request)
    {
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->getAccessToken();
        $response = $provider->capturePaymentOrder($request['token']);
        $dataCreate = $request->except(['paymentt']);
        $dataCreate['user_id'] = auth()->user()->id;
        $dataCreate['payment'] = "paypal";
        $dataCreate['status'] = 'Accept';
        $this->order->create($dataCreate);
        $cart = $this->cart->firtOrCreateBy(auth()->user()->id);
        $cart->products()->delete();
        Session::forget(['coupon_id', 'discount_amount_price', 'coupon_code']);

        if (isset($response['status']) && $response['status'] == 'COMPLETED') {
            return redirect()
                ->route('client.orders.index')
                ->with('success', 'Transaction complete');
        } else {
            return redirect()
                ->route('client.orders.index')
                ->with('error', $response['message'] ?? 'Something went wrong.');
        }
    }

    public function cancelTransaction(Request $request)
    {
        return redirect()
            ->route('client.orders.index')
            ->with('error', $response['message'] ?? 'You have canceled the transaction.');
    }
}
