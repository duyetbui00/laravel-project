<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ProductDetail;
use Illuminate\Http\Request;
use App\Models\Product;



class ProductController extends Controller
{

    protected $product;
    protected $productDetail;
    protected $category;
    public function __construct(Product $product,ProductDetail $productDetail,Category $category)
    {
        $this->product = $product;
        $this->productDetail = $productDetail;
        $this->category = $category;

    }

    public function index(Request $request, $category_id)
    {
        $categories=$this->category->all();
        $products =  $this->product->getBy($request->all(), $category_id)->Size()->Search()->SortBy()->paginate(6);
        $productDetails= $this->productDetail->select('size')->distinct()->get();
        return view('client.products.index', compact('products','productDetails','categories'));
    }

    public function show($id)
    {
        $product = $this->product->with('details')->findOrFail($id);
        return view('client.products.detail', compact('product'));

    }
}
