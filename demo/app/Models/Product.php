<?php

namespace App\Models;

use App\Traits\HandleImageTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory, HandleImageTrait;

    protected $fillable = [
        'name',
        'description',
        'sale',
        'price'
    ];

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function assignCategory($categoryIds)
    {
        return $this->categories()->sync($categoryIds);
    }

    public function getBy($dataSearch, $categoryId)
    {
        return $this->whereHas('categories', fn ($q) => $q->where('category_id', $categoryId));
    }

    public function getImagePathAttribute()
    {
        return asset($this->images->count() > 0 ? 'upload/' . $this->images->first()->url : 'upload/default.jpg');
    }

    public function getSalePriceAttribute()
    {
        return $this->attributes['sale'] ? $this->attributes['price'] - ($this->attributes['sale'] * 0.01  * $this->attributes['price']) : 0;
    }
    public function scopeSearch($query)
    {
        if (request()->key) {
            $key = request()->key;
            $query->where('name', 'LIKE', '%' . $key . '%');
        }
        return $query;
    }

    public function scopeSize($query)
    {
        if (request()->size) {
            $query->whereHas('details', function ($query) {
                $query->whereIn('product_details.size', request()->size);
            });
        }
        return $query;
    }

    public function scopeSortBy($query)
    {
        if (request()->sort == "DESC") {
            $query->orderByRaw('(price - (price*sale/100)) desc');
        }
        if (request()->sort == "ASC") {
            $query->orderByRaw('(price - (price*sale/100)) asc');
        }
        return $query;
    }
}
