$(function () {
    getTotalValue()
    getCompare()
    function getTotalValue() {
        let total = $('.total-price').data('price')
        let couponPrice = $('.coupon-div')?.data('price') ?? 0;
        $('.total-price-all').text(`$${total - couponPrice}`)
    }

    function getCompare(){
        let total = $('.total-price').data('price')
        let couponPrice = $('.coupon-div')?.data('price') ?? 0;
        if(total <= couponPrice ){
            $('.total-price-all').text(`0$`)
        }
    }

    const TIME_TO_UPDATE = 1500;
    //Remove
    $(document).on('click', '.btn-remove-product', _.debounce(function (e) {
        let url = $(this).data('action')
        $.post(url, res => {
            let total = $('.total-price').data('price');
            let couponPrice = $('.coupon-div')?.data('price') ?? 0;
            let cart = res.cart;
            let cartProductId = res.product_cart_id;
            $('#productCountCart').text(cart.product_count)
            $('.total-price').text(`$${cart.total_price}`).data('price', cart.product_count)
            $(`#row-${cartProductId}`).remove();
            if ($('.coupon-div')?.data('price')==null){
                $('.total-price-all').text(`$${cart.total_price}`);
            }else if(total <= couponPrice){
                $('.total-price-all').text(`0$`);
            }
            else{
                $('.total-price-all').text(`$${cart.total_price -  $('.coupon-div')?.data('price')}`);
            }
            Swal.fire({
                position: "center",
                icon: "success",
                title: "success",
                showConfirmButton: false,
            },setTimeout(function(){
                window.location.reload();
            }, 1500));
        })
    }, TIME_TO_UPDATE))
    //Update
    $(document).on('click', '.btn-update-quantity', _.debounce(function (e) {
        let url = $(this).data('action')
        let id = $(this).data('id')
        let data = {
            product_quantity: $(`#productQuantityInput-${id}`).val()
        }
        $.post(url, data, res => {
            let total = $('.total-price').data('price');
            let couponPrice = $('.coupon-div')?.data('price') ?? 0;
            let cartProductId = res.product_cart_id;
            let cart = res.cart;
            $('#productCountCart').text(cart.product_count)
            if (res.remove_product) {
                $(`#row-${cartProductId}`).remove();
            } else {
                $(`#cartProductPrice${cartProductId}`).html(
                    `$${res.cart_product_price}`);
            }
            $('.total-price').text(`$${cart.total_price}`);
            if ($('.coupon-div')?.data('price')==null){

                $('.total-price-all').text(`$${cart.total_price}`);
            }else if(total <= couponPrice){
                $('.total-price-all').text(`0$`);
            }
            else {
                $('.total-price-all').text(`$${cart.total_price -  $('.coupon-div')?.data('price')}`);
            }
            Swal.fire({
                position: "center",
                icon: "success",
                title: "success",
                showConfirmButton: false,
            },setTimeout(function(){
                window.location.reload();
            }, 1500));
        })
    }, TIME_TO_UPDATE))

});
