@extends('admin.layouts.app')
@section('title', 'Edit Category ' . $category->name)
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">Edit Category</h2>
        <div>
            <form action="{{ route('categories.update', $category->id) }}" method="post">
                @csrf
                @method('put')
                <div class="input-group input-group-static">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') ?? $category->name }}" name="name" class="form-control">
                    @error('name')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                @if ($category->childrens->count() < 1)
                    <div class="input-group input-group-static">
                        <label name="group" class="ms-0">Parent Category</label>
                        <select name="parent_id" class="form-control">
                            <option value=""> Select Parent Category</option>
                            @foreach ($parentCategories as $item)
                                <option value="{{ $item->id }}"
                                    {{ (old('parent_id') ?? $category->parent_id) == $item->id ? 'selected' : '' }}>
                                    {{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <button style="color: #1d1c1d;" type="submit" class="btn btn-submit bg-gradient-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

