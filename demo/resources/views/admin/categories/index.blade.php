@extends('admin.layouts.app')
@section('title', 'Categories')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">
            Category list
        </h2>
        <div class="d-flex create">
            @can('create-category')
                <a href="{{ route('categories.create') }}" class="btn btn-success btn-outline-success"><i
                        class="fa-solid fa-plus"></i></a>
            @endcan
            @if (session('message'))
                <h2 style="padding-left: 15px" class="text-primary">{{ session('message') }}</h2>
            @endif
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Parent Name</th>
                    <th>Action</th>
                </tr>
                @foreach ($categories as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->parent_name }}</td>
                        <td class="d-flex">
                            @can('update-category')
                                <div class="edit">
                                    <a href="{{ route('categories.edit', $item->id) }}"
                                       class="btn btn-outline-info btn-info"><i
                                            class="fa-solid fa-pencil"></i></a>
                                </div>
                            @endcan
                            @can('delete-category')
                                <form action="{{ route('categories.destroy', $item->id) }}"
                                      id="form-delete{{ $item->id }}" method="post">
                                    @csrf
                                    @method('delete')
                                </form>
                                <button class="btn btn-delete btn-danger btn-outline-danger" data-id={{ $item->id }}><i
                                        class="fa-solid fa-minus"></i></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $categories->links() }}
        </div>
    </div>
@endsection
@section('script')
    <script></script>
@endsection

