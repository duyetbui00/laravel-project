@extends('admin.layouts.app')
@section('title', 'Create Coupon')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">Create Coupon</h2>
        <div>
            <form action="{{ route('coupons.store') }}" method="post">
                @csrf
                <div class="input-group input-group-static ">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control"
                           style="text-transform: uppercase">
                    @error('name')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label>Value</label>
                    <input type="number" min="0" max="100" value="{{ old('value') }}" name="value" class="form-control">
                    @error('value')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label name="group" class="ms-0">Type</label>
                    <select name="type" class="form-control">
                        <option> Select Type</option>
                        <option value="money" selected> Money</option>
                    </select>
                </div>
                @error('type')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
                <div class="input-group input-group-static ">
                    <label>Expiry date</label>
                    <input type="date" value="{{ old('expery_date') }}" name="expery_date" class="form-control">
                    @error('expery_date')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <button style="color: #1d1c1d;" type="submit" class="btn btn-submit bg-gradient-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
