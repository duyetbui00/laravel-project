@extends('admin.layouts.app')
@section('title', 'Coupons')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">
            Coupon list
        </h2>
        <div class="d-flex create">
            @can('create-coupon')
                <a href="{{ route('coupons.create') }}" class="btn btn-success btn-outline-success"><i
                        class="fa-solid fa-plus"></i></a>
            @endcan
            @if (session('message'))
                <h2 style="padding-left: 15px" class="text-primary">{{ session('message') }}</h2>
            @endif
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Value</th>
                    <th>Expiry Date</th>
                    <th>Action</th>
                </tr>
                @foreach ($coupons as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->value }}</td>
                        <td>{{ $item->expery_date }}</td>
                        <td class="d-flex">
                            @can('update-coupon')
                                <div class="edit">
                                    <a href="{{ route('coupons.edit', $item->id) }}"
                                       class="btn btn-outline-info btn-info"><i
                                            class="fa-solid fa-pencil"></i></a>
                                </div>
                            @endcan
                            @can('delete-coupon')
                                <form action="{{ route('coupons.destroy', $item->id) }}" id="form-delete{{ $item->id }}"
                                      method="post">
                                    @csrf
                                    @method('delete')
                                </form>
                                <button class="btn btn-delete btn-danger btn-outline-danger" data-id={{ $item->id }}><i
                                        class="fa-solid fa-minus"></i></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $coupons->links() }}
        </div>
    </div>
@endsection
@section('script')
    <script></script>
@endsection
