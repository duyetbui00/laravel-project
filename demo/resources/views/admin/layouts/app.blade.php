<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="./assets/img/favicon.png">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @yield('title', 'Dashboard')
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('admin/assets/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin/assets/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/682371469c.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('admin/assets/css/material-dashboard.css?v=3.0.2') }}" rel="stylesheet" />
    <style>
        .bg-gradient-primary{
        background-image:linear-gradient(195deg, rgb(177 239 181) 30%, rgb(241 183 119) 70%)
        }
        .sidenav .collapse .nav-item .nav-link.active {
            color: #222224 !important;
        }
        .table> :not(caption)>*>* {
            padding: 0.3rem 0.3rem;
        }
        .edit{
            padding-right: 5px;
        }
    </style>
    @yield('style')
</head>
<body class="g-sidenav-show  bg-gray-200">
@include('admin.layouts.sidebar')
<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    @include('admin.layouts.navbar')
    <!-- End Navbar -->
    <div class="container-fluid ">
        <div class="row">
            @yield('content')
        </div>
        @include('admin.layouts.footer')
    </div>
</main>
<!--   Core JS Files   -->
@include('admin.layouts.javascript')
</body>
</html>
