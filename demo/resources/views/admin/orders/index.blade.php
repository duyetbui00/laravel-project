@extends('admin.layouts.app')
@section('title', 'orders')
@section('content')
    <div class="card">
        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif
        <h2 style="text-align: center;padding:2px">
            Orders
        </h2>
        <div class="container-fluid ">
            @if (session('message'))
                <h1 class="text-primary">{{ session('message') }}</h1>
            @endif
            <div class="col card">
                <div>
                    <table class="table table-hover">
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Customer Address</th>
                            <th>Note</th>
                            <th>Payment</th>
                            <th>Ship</th>
                            <th>Total</th>
                            <th>Status</th>
                        </tr>
                        @foreach ($orders as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->customer_name }}</td>
                                <td>{{ $item->customer_email }}</td>
                                <td style="overflow: hidden;text-overflow: ellipsis;max-width: 16ch;white-space: nowrap;">{{ $item->customer_address }}</td>
                                <td style="overflow: hidden;text-overflow: ellipsis;max-width: 16ch;white-space: nowrap;">{{ $item->note }}</td>
                                <td>{{ $item->payment }}</td>
                                <td>${{ $item->ship }}</td>
                                <td>${{ $item->total }}</td>
                                <td>
                                    <div class="input-group input-group-static mb-4">
                                        <select name="status" class="form-control select-status"
                                                data-action="{{ route('admin.orders.update_status', $item->id) }}">
                                            @foreach (config('order.status') as $status)
                                                <option value="{{ $status }}"
                                                    {{ $status == $item->status ? 'selected' : '' }}>{{ $status }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(function () {
            $(document).on("change", ".select-status", function (e) {
                e.preventDefault();
                let url = $(this).data("action");
                let data = {
                    status: $(this).val(),
                    _token: "{{ csrf_token() }}",
                };
                $.post(url, data, res => {
                    Swal.fire({
                        position: "center",
                        icon: "success",
                        title: "success",
                        showConfirmButton: false,
                        timer: 1500,
                    });
                });
            });
        });
    </script>
@endsection

