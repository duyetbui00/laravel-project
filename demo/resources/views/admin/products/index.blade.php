@extends('admin.layouts.app')
@section('title', 'Products')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">
            Products list
        </h2>
        <div class="create d-flex">
            @can('create-product')
                <a href="{{ route('products.create') }}" class="btn btn-success btn-outline-success"><i
                        class="fa-solid fa-plus"></i></a>
            @endcan
            @if (session('message'))
                <h2 style="padding-left: 15px" class="text-primary">{{ session('message') }}</h2>
            @endif
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Sale</th>
                    <th>Action</th>
                </tr>
                @foreach ($products as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><img src="{{ $item->image_path }}" width="77px" height="77px" alt=""></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->sale }}</td>
                        <td class="d-flex">
                            @can('update-product')
                                <div class="edit">
                                    <a href="{{ route('products.edit', $item->id) }}"
                                       class="btn btn-outline-info btn-info"><i
                                            class="fa-solid fa-pencil"></i></a>
                                </div>
                            @endcan
                            @can('delete-product')
                                <div class="edit">
                                    <form action="{{ route('products.destroy', $item->id) }}"
                                          id="form-delete{{ $item->id }}"
                                          method="post">
                                        @csrf
                                        @method('delete')
                                    </form>
                                    <button class="btn btn-delete btn-danger btn-outline-danger"
                                            data-id={{ $item->id }}><i
                                            class="fa-solid fa-minus"></i></button>
                                </div>
                            @endcan
                            @can('show-product')
                                <a href="{{ route('products.show', $item->id) }}"
                                   class="btn btn-secondary btn-outline-secondary"><i
                                        class="fa-solid fa-eye"></i></a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $products->links() }}
        </div>
    </div>
@endsection
@section('script')
@endsection
