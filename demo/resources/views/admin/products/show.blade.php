@extends('admin.layouts.app')
@section('title', 'Show Product')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">Show Product</h2>
        <div>
            <div class="row">
                <div class=" ">
                    <p>Image</p>
                    <div class="col-5">
                        <img
                            src="{{ $product->images ? asset('upload/' . $product->images->first()->url) : 'upload/default.jpg' }}"
                            id="show-image" alt="Product image" width="250px" height="250px">
                    </div>
                </div>
                <div class="4">
                    <p>Name : {{ $product->name }}</p>
                </div>
                <div class="">
                    <p>Price: {{ $product->price }}</p>
                </div>
                <div class="">
                    <p>Sale: {{ $product->sale }}</p>
                </div>
                <div class="form-group">
                    <p>Description</p>
                    <div class="row w-100 h-100">
                        {!! $product->description !!}
                    </div>
                </div>
                <div>
                    <p>Size</p>
                    @if ($product->details->count() > 0)
                        @foreach ($product->details as $detail)
                            <p>Size: {{ $detail->size }} - quantity: {{ $detail->quantity }}</p>
                        @endforeach
                    @else
                        <p> This product has not entered the size</p>
                    @endif
                </div>
            </div>
            <div>
                <p>Category</p>
                @foreach ($product->categories as $item)
                    <p>{{ $item->name }}</p>
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection
