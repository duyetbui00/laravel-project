@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">
            Role list
        </h2>
        <div class="create d-flex">
            <a href="{{ route('roles.create') }}" class="btn btn-outline-success btn-success"><i class="fa-solid fa-plus"></i></a>
            @if (session('message'))
                <h2 style="padding-left: 15px" class="text-primary">{{ session('message') }}</h2>
            @endif
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>DisplayName</th>
                    <th>Action</th>
                </tr>
                @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td class="d-flex">
                            <div class="edit">
                            <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-outline-info btn-info"><i class="fa-solid fa-pencil"></i></a>
                            <form action="{{ route('roles.destroy', $role->id) }}"
                                  id="form-delete{{ $role->id }}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                            </div>
                            <button class="btn btn-delete btn-outline-danger btn-danger" data-id={{ $role->id }}><i class="fa-solid fa-minus"></i></button>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $roles->links() }}
        </div>
    </div>
@endsection
@section('script')
    <script>
    </script>
@endsection
