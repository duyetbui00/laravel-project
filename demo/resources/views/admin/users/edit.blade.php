@extends('admin.layouts.app')
@section('title', 'Edit user ' . $user->name)
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">Update user</h2>
        <div>
            <form action="{{ route('users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="row">
                    <div class=" input-group-static col-5 ">
                        <label>Image</label>
                        <input type="file" accept="image/*" name="image" id="image-input" class="form-control">
                        @error('image')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        <img src="{{$user->image_path}}" id="show-image" alt="User image" width="150px" height="150px">
                    </div>
                </div>
                <div class="input-group input-group-static">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') ?? $user->name }}" name="name" class="form-control">
                    @error('name')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static">
                    <label>Email</label>
                    <input type="email" value="{{ old('email') ?? $user->email }}" name="email" class="form-control">
                    @error('email')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label>Phone</label>
                    <input type="text" value="{{ old('phone') ?? $user->phone }}" name="phone" class="form-control">
                    @error('phone')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label name="group" class="ms-0">Gender</label>
                    <select name="gender" class="form-control" value={{ $user->gender }}>
                        <option value="male">Male</option>
                        <option value="fe-male">FeMale</option>
                    </select>
                    @error('gender')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label>Address</label>
                    <textarea name="address" class="form-control">{{ old('address') ?? $user->address }} </textarea>
                    @error('address')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="input-group input-group-static ">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                    @error('password')
                    <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Roles</label>
                    <div class="row">
                        @foreach ($roles as $groupName => $role)
                            <div class="col-5">
                                <h4>{{ $groupName }}</h4>
                                <div class="d-flex" style="flex-wrap: wrap;">
                                    @foreach ($role as $item)
                                        <div class="form-check" style="width: 131px;padding-left: 0px">
                                            <input class="form-check-input" name="role_ids[]"
                                                   {{ $user->roles->contains('id', $item->id) ? 'checked' : '' }}
                                                   type="checkbox" value="{{ $item->id }}">
                                            <label class="custom-control-label"
                                                   for="customCheck1">{{ $item->display_name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <button style="color: #1d1c1d;" type="submit" class="btn btn-submit bg-gradient-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>
    <script>
        $(() => {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#show-image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image-input").change(function () {
                readURL(this);
            });
        });
    </script>
@endsection
