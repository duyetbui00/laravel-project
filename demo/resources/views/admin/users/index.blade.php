@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
    <div class="card">
        <h2 style="text-align: center;padding:2px">
            User list
        </h2>
        <div class="create d-flex">
            @can('create-user')
                <a href="{{ route('users.create') }}" class="btn btn-outline-success btn-success"><i
                        class="fa-solid fa-plus"></i></a>
            @endcan
            @if (session('message'))
                <h2 style="padding-left: 15px" class="text-primary">{{ session('message') }}</h2>
            @endif
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                @foreach ($users as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><img src="{{ $item->image_path }}" width="77px" height="77px" alt=""></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->phone }}</td>
                        <td class="d-flex">
                            @can('update-user')
                                <div class="edit">
                                    <a href="{{ route('users.edit', $item->id) }}"
                                       class="btn btn-outline-info btn-info"><i class="fa-solid fa-pencil"></i></a>
                                </div>
                            @endcan
                            @can('delete-user')
                                <form action="{{ route('users.destroy', $item->id) }}" id="form-delete{{ $item->id }}"
                                      method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-delete btn-outline-danger btn-danger" type="submit"
                                            data-id={{ $item->id }}><i class="fa-solid fa-minus"></i></button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $users->links() }}
        </div>
    </div>
@endsection
@section('script')
@endsection
