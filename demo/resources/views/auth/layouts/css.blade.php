<style>
    .gradient-custom {
        /* fallback for old browsers */
        background: #6a11cb;
        /* Chrome 10-25, Safari 5.1-6 */
        /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
        background: linear-gradient(to right, rgb(241 183 119), rgb(177 239 181))
    }
    .form-control-lg {
        /* text-align: center; */
        min-height: calc(1.5em + 1rem + 2px);
        padding: 0.5rem 1rem;
        font-size: 1rem;
        border-radius: 0.65rem;
    }
    .form-control {
        /* text-align: center; */
        width:50%;
    }
    .text-md-end {
        text-align: left!important;
    }
    .dark{
        padding-top: 10px;
    }
    .left{
        text-align: left;
    }
    a{
        padding-left: 10px;
    }
</style>
