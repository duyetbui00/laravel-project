@extends('auth.layouts.app')
@section('content')
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="card-body p-3 text-center">
            <div class="mt-md-4 pb-3">
                <h3 class="fw-bold text-uppercase">LOGIN</h3>
                <div class="form-outline form-white d-flex mb-4" >
                    <label class="col-md-3 col-form-label text-md-end" >Email</label>
                    <div class="col-md-12">
                    <input type="email" id="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus />
                    @error('email')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                    </div>
                </div>
                <div class="form-outline form-white d-flex mb-4">
                    <label class="col-md-3 col-form-label text-md-end"> Password</label>
                    <div class="col-md-12">
                    <input type="password" id="password" class="form-control form-control-lg" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" />
                    @error('password')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                    </div>
                </div>
                <div class="dark">
                    <button class="btn btn-outline-light btn-lg " type="submit">Login</button>
                    <a class="text-white-50" href="{{ route('register') }}">Click here to register</a>
                </div>
        </div>
        </div>
    </form>
@endsection
