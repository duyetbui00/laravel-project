@extends('auth.layouts.app')
@section('title', 'Register')
@section('content')
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="card-body p-3 text-center">
            <h3 class="fw-bold text-uppercase">REGISTER</h3>
            <div class="form-outline form-white d-flex mb-4" >
                <label class="col-md-4 col-form-label text-md-end" >Name</label>
                <div class="col-md-12">
                    <input type="text" id="name" class="form-control form-control-lg @error('name')
                    is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus />
                    @error('name')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-outline form-white d-flex mb-4" >
                <label class="col-md-4 col-form-label text-md-end" >Email Address</label>
                <div class="col-md-12">
                    <input type="email" id="email" class="form-control form-control-lg @error('email')
                    is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"  />
                    @error('email')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-outline form-white d-flex mb-4" >
                <label class="col-md-4 col-form-label text-md-end" >Phone</label>
                <div class="col-md-12">
                    <input type="number" id="phone" class="form-control form-control-lg @error('phone')
                    is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone"  />
                    @error('phone')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-outline form-white d-flex mb-4" >
                <label class="col-md-4 col-form-label text-md-end" >Gender</label>
                <div class="col-md-12">
                    <select name="gender"  class="form-control">
                        <option value="male">Male</option>
                        <option value="fe-male">FeMale</option>
                    </select>
                    @error('gender')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-outline form-white d-flex mb-4" >
                <label class="col-md-4 col-form-label text-md-end" >Password</label>
                <div class="col-md-12">
                    <input type="password" id="password" class="form-control form-control-lg @error('password')
                    is-invalid @enderror" name="password"  required autocomplete="new-password"  />
                    @error('password')
                    <span class="invalid-feedback text-white left" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
            </div>
            <div class="form-outline form-white d-flex mb-2" >
                <label class="col-md-4 col-form-label text-md-end" >Confirm Password</label>
                <div class="col-md-12">
                    <input type="password" id="password-confirm" class="form-control form-control-lg"
                           name="password_confirmation" required autocomplete="new-password" />
                </div>
            </div>
            <div class="dark">
                <button class="btn btn-outline-light btn-lg " type="submit">Register</button>
                <a class="text-white-50" href="{{ route('login') }}">Click here to login</a>
            </div>
        </div>
    </form>
@endsection
