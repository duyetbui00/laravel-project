@extends('client.layouts.app')
@section('title', 'Home')
@section('content')
    <style>
        .contact-us {
            padding: 70px 0px;
        }
        .section-title h2 {
            color: #ffffff;
            font-size: 30px;
            font-weight: normal;
            text-transform: uppercase;
            letter-spacing: 8px;
            border-bottom: 5px solid #af8070;
            padding-bottom: 10px;
            display: inline-block;
        }
        .section-title p {
            color: #e8e8e8;
            margin-bottom: 30px;
        }
        .contact-us input,
        .contact-us textarea {

            background: #fff;
        }
        .contact-us textarea {
            min-height: 188px;
            max-width: 100%;
        }
        .contact-us .map {
            overflow: hidden;
            border-radius: 4px;
        }
        .col-7{
            border-radius: 7%;
            height: 496px;
            background: linear-gradient(to right, rgb(241 183 119), rgb(177 239 181));
        }
        .zo{
            padding-left: 39px;
            max-width: 595.2px;
            padding-top: 71px;
        }
    </style>
</head>

<!-- Contact Us Section -->
<section class="contact-us">
    <div class="container">
        <div class="row">
            <div class="col-7">
                <form action="/" class="mb-4 mb-lg-0 zo">
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" />
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Type Message"></textarea>
                    </div>
                    <button type="submit" class="btn btn-light">Contact Now</button>
                </form>
            </div>

            <div class="col-lg-5">
                <div class="map">
                    <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.4981887574572!2d107.58395981429213!3d16.450292888646445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a1457de005d7%3A0x6608565df1f87298!2zMTc2IFRy4bqnbiBQaMO6LCBQaMaw4bubYyBWxKluaCwgVGjDoG5oIHBo4buRIEh14bq_LCBUaOG7q2EgVGhpw6puIEh14bq_IDUzMDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1661484470085!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border: 0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

