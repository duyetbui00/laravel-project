<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
    <div class="row px-xl-5 pt-5">
        <div class="col-lg-3 col-md-12 mb-5 pr-3 pr-xl-5">
            <a href="" class="text-decoration-none">
                <h1 class="mb-4 display-5 font-weight-semi-bold"><span
                        class="text-primary font-weight-bold border  px-3 mr-1">Rayz</span>Shopper</h1>
            </a>
            <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>Hue</p>
            <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>rayz@gmail.com</p>
            <p class="mb-0"><i class="fa fa-phone-alt text-primary mr-3"></i>+123456789</p>
        </div>
        <div class="col-lg-3 col-md-12 mb-5 pr-3 pr-xl-5">
            <p style="padding-top:72px" class="mb-2"><i class="fa-brands fa-square-instagram text-primary mr-3"></i>instagram.com/__rayy_z</p>
            <p class="mb-2"><i class="fa-brands fa-facebook text-primary mr-3"></i>facebook.com/bc.rayzd</p>
            <p class="mb-0"><i class="fa-brands fa-tiktok text-primary mr-3"></i>tiktok.com/@__rayy_z</p>
        </div>
        <div class="col-lg-6 col-md-12" >
            <div class="row">
                <div class="col-md-6 mb-5">
                    <h5 class="font-weight-bold text-dark mb-4">Quick Links</h5>
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-dark mb-2" href="{{ route('client.home') }}"><i
                                class="fa fa-angle-right mr-2"></i>Home</a>

                        <a class="text-dark mb-2" href="#"><i
                                class="fa fa-angle-right mr-2"></i>Checkout</a>
                        <a class="text-dark" href="{{ route('client.contact') }}"><i class="fa fa-angle-right mr-2"></i>Contact
                            Us</a>
                    </div>
                </div>
                <div class="col-md-6 mb-5">
                    <h5 class="font-weight-bold text-dark mb-4">Newsletter</h5>
                    <form action="">
                        <div class="form-group">
                            <input type="text" class="form-control border-0 py-4" placeholder="Your Name"
                                   required="required" />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control border-0 py-4" placeholder="Your Email"
                                   required="required" />
                        </div>
                        <div>
                            <button class="btn btn-primary btn-block border-0 py-3" type="submit">Subscribe
                                Now</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row border-top border-light mx-xl-5 py-4">
        <div class="col-md-6 px-xl-0">
            <a class="mb-md-0 text-center text-md-left text-dark">
                &copy; <a class="text-dark font-weight-semi-bold" href="#">Rayz Shop</a>. All Rights Reserved.
                Designed
                by
                <a class="text-dark font-weight-semi-bold" href="https://htmlcodex.com">Rayz
            </a>
        </div>
        <div class="col-md-6 px-xl-0 text-center text-md-right">
            <img class="img-fluid" src="{{ asset('client/img/payments.png') }}" alt="">
        </div>
    </div>
</div>
