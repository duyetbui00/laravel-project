<!-- Featured Start -->
@extends('client.layouts.app')
@section('title', 'Home')
@section('content')
    <div class="container-fluid pt-5">
        @if (session('message'))
            <div class="alert alert-success" style="font-size: 20px;text-align: center">{{ session('message') }}</div>
        @endif
        @if(\Session::has('error'))
                <div class="alert alert-danger" style="font-size: 20px;text-align: center">{{ \Session::get('error') }}</div>
                {{ \Session::forget('error') }}
         @endif
         @if(\Session::has('success'))
                <div class="alert alert-success" style="font-size: 20px;text-align: center">{{ \Session::get('success') }}</div>
                {{ \Session::forget('success') }}
         @endif
        <div class="col">
            <div>
                <table class="table table-hover">
                    <tr>
{{--                        <th>#</th>--}}
                        <th>Customer Name</th>
                        <th>Customer Email</th>
                        <th>Customer Address</th>
                        <th>Note</th>
                        <th>Payment</th>
                        <th>Ship</th>
                        <th>Total</th>
                        <th>Status</th>
                    </tr>

                    @foreach ($orders as $item)
                        <tr>
{{--                            <td>{{ $item->id }}</td>--}}
                            <td>{{ $item->customer_name }}</td>
                            <td>{{ $item->customer_email }}</td>
                            <td style="overflow: hidden;text-overflow: ellipsis;max-width: 16ch;white-space: nowrap;">{{ $item->customer_address }}</td>
                            <td style="overflow: hidden;text-overflow: ellipsis;max-width: 16ch;white-space: nowrap;">{{ $item->note }}</td>
                            <td>{{ $item->payment }}</td>
                            <td>${{ $item->ship }}</td>
                            <td>${{ $item->total }}</td>
                            <td>{{ $item->status }}</td>
                            <td>
                                @if ($item->status == 'Pending')
                                    <form action="{{ route('client.orders.cancel', $item->id) }}"
                                          id="form-cancel{{ $item->id }}" method="post">
                                        @csrf
                                        <button class="btn btn-cancel btn-danger" data-id={{ $item->id }}>Cancle
                                            Order</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
                {{ $orders->links() }}
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(function() {
            $(document).on("click", ".btn-cancel", function(e) {
                e.preventDefault();
                let id = $(this).data("id");
                Swal.fire({
                    position: "center",
                    icon: "success",
                    title: "success",
                    showConfirmButton: false,
                    timer: 1500,
                })
                    .then(function() {
                        $(`#form-cancel${id}`).submit();
                    })
                    .catch();
            })
        });
    </script>

@endsection
