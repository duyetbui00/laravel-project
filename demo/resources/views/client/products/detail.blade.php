@extends('client.layouts.app')
@section('title', 'product detail')
@section('content')
    <!-- Page Header Start -->
    <div class="row" style="margin-left: 50px">
        <div class="d-inline-flex">
            <p class="m-0"><a href="">Home</a></p>
            <p class="m-0 px-2">/</p>
            <p class="m-0">Shop Detail</p>
        </div>
    </div>
    <!-- Page Header End -->
    @if (session('message'))
        <h2 class="" style="text-align: center; width:100%; color:red"> {{ session('message') }}</h2>
    @endif

    <!-- Shop Detail Start -->
    <div class="container-fluid py-5">
        <form action="{{ route('client.carts.add') }}" method="POST" class="row px-xl-5">
            @csrf
            <input type="hidden" name="product_id" value="{{ $product->id }}">
            <div class="col-lg-5 pb-5" style="max-width:31.66667% ;">
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <h3 class="font-weight-semi-bold">{{ $product->name }}</h3>
                    <div class="carousel-inner border">
                        <div class="carousel-item active">
                            <img class="w-100 h-100"
                                 src="{{ $product->images->count() > 0 ? asset('upload/' . $product->images->first()->url) : 'upload/default.png' }}"
                                 alt="Image">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-7 pb-5">
                <div class="d-flex mb-4 " style="padding-bottom:114.5px">
                </div>
                <div class="d-flex">
                @if($product->sale==0)
                    <h4 class="font-weight-semi-bold mb-4">${{ $product->price }}</h4>
                @endif
                @if($product->sale>0)
                    <h3 class="text-muted "><del>${{ $product->price }}</del></h3>
                    <h3 class="ml-2">${{$product->price -($product->sale/100*$product->price)}}</h3>
                @endif
                </div>
                <div class="d-flex mb-4">
                    <h5 class="text-dark font-weight-medium mb-0 mr-3">Size:</h5>
                    @if ($product->details->count() > 0)
                        <form>
                            @foreach ($product->details as $size)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="product_size"
                                           value="{{ $size->size }}" id="size{{ $size->size }}">
                                    <label for="size{{ $size->size }}"
                                           class="
                                        custom-control-label">{{ $size->size }}</label>

                                </div>
                            @endforeach
                        </form>
                    @else
                        <p>Out of stock</p>
                    @endif
                </div>
                <div class="d-flex align-items-center mb-4 pt-2">
                    <div class="input-group quantity mr-3" style="width: 130px;">
                        <input type="number" name="product_quantity" class="form-control bg-secondary text-center" value="1" min="1"   oninput="validity.valid||(value='1');">
                    </div>
                    <button class="btn btn-primary px-3"><i class="fa fa-shopping-cart mr-1"></i> Add To Cart</button>
                </div>
                <div class="d-flex pt-2">
                    <p class="text-dark font-weight-medium mb-0 mr-2">Share on:</p>
                    <div class="d-inline-flex">
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                        <a class="text-dark px-2" href="">
                            <i class="fab fa-pinterest"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <div class="row px-xl-5">
            <div class="col">
                <div class="nav nav-tabs justify-content-center border-secondary mb-4">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#tab-pane-1">Description</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        {!! $product->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
