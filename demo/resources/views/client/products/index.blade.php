<!-- Page Header Start -->
@extends('client.layouts.app')
@section('title', 'products')
@section('content')
    <div class="row ">
        <div class="d-inline-flex" style="margin-left:40px">
            <p class="m-0"><a href="">Home</a></p>
            <p class="m-0 px-2">/</p>
            <p class="m-0">Shop</p>
        </div>
    </div>
    <!-- Page Header End -->
    <!-- Shop Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5">
            <!-- Shop Sidebar Start -->
            <div class="col-lg-3 col-md-12">
                <!-- Size Start -->
                <form method="GET">
                <div class="border-bottom mb-4 pb-4">
                    <div class="d-flex ">
                        <h5 class="font-weight-semi-bold mb-4">Filter by size</h5>
                        <button class="font-weight-semi-bold mb-4" type="submit"> <i class="fa-solid fa-filter"></i> </button>
                    </div>
                        @foreach($productDetails as $aa)
                        <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                            <input type="checkbox"  class="custom-control-input" value="{{$aa->size}}" id="{{$aa->size}}" name="size[]" >
                                <label class="custom-control-label"  for="{{$aa->size}}">{{$aa->size}}</label>
                        </div>
                        @endforeach
                </div>
                </form>
                <!-- Color End -->
            </div>
            <!-- Shop Sidebar End -->
            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-12">
                <div class="row pb-3">
                    <div class="col-12 pb-1">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <form action="" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="key" placeholder="Search by name">
                                    <div class="input-group-append">
                                        <button type="submit" class="input-group-text bg-transparent text-primary">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <form method="GET">
                            <div class="dropdown ml-4 d-flex"   >
                                <select  name="sort" class="form-control" onchange='if(this.value != "0") { this.form.submit(); }'>
                                    <option  value="" @if(request()->sort == "") selected @endif > Sort by</option>
                                    <option id="ASC" value="ASC" @if(request()->sort=="ASC") selected @endif>ASC</option>
                                    <option value="DESC" @if(request()->sort=="DESC") selected @endif>DESC</option>
                                </select>
                                {{-- <button type="submit"><i class="fa-solid fa-filter"></i></button> --}}

                            </div>
                            </form>
                        </div>
                    </div>
                    @foreach ($products as $item)
                        <div class="col-lg-4 col-md-6 col-sm-12 pb-1">
                            <div class="card product-item border-0 mb-4">
                                <div
                                    class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                                    <a href="{{ route('client.products.show', $item->id) }}"><img class="img-fluid w-100"
                                         src="{{ $item->images->count() > 0 ? asset('upload/' . $item->images->first()->url) : 'upload/default.jpg' }}"
                                         alt="">
                                    </a></div>
                                <div class="card-body border-left border-right text-center p-0 pt-4 pb-3">
                                    <h6 class="text-truncate mb-3">{{ $item->name }}</h6>
                                    <div class="d-flex justify-content-center">
                                        @if($item->sale==0)
                                            <h6>${{ $item->price }}</h6>
                                        @endif
                                        @if($item->sale>0)
                                            <h6 class="text-muted "><del>${{ $item->price }}</del></h6>
                                            <h6 class="ml-2">${{$item->price -($item->sale/100*$item->price)}}</h6>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 pb-1">
                        {{ $products->appends(request()->all())->links() }}

                    </div>
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
@endsection

